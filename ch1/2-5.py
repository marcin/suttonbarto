#!/usr/bin/python3 -tt

"""
implementation fo the 10-armed testbed from Sutton & Barto
"""

### import libs
import numpy as np
import sys
import os
from matplotlib import pyplot as plt

np.set_printoptions(precision=2)

def save_agent_data(no_arms, no_steps, no_agents, epsilon_steps, path):

  agents_data = np.empty((len(epsilon_steps), no_agents, no_steps, 2))

  epsilons, agents, steps = np.nested_iters(agents_data, [[0], [1], [2]], flags = ["multi_index"], op_flags = [["writeonly"]])

  for eps in epsilons:
    eps_current = epsilon_steps[epsilons.multi_index[0]]
    print("Epsilon:", eps_current)
    # for each agent
    for agent in agents:

      q_star = np.random.randn(no_arms)

      #print("Agent %d" % agents.multi_index[0])
      # first, get matrix of all random exploration vs exploitation choices
      choices = np.random.choice([1, 0], size = no_steps, p = [1-eps_current, eps_current])
      #print("Choices I'll follow", str(choices))

      N = np.zeros(no_arms)
      Q = np.zeros(no_arms)


      for step in steps:

        #print("Step %d" % steps.multi_index[0])

        #print('Values', str(Q))
        # if we choose to exploit, pick maximum value
        if choices[steps.multi_index[0]]:
          #print("Exploiting")
          # get vector of best values
          max_value = np.argwhere(Q == np.amax(Q))
          # if there is more than one best action
          if max_value.shape[0] > 1:
            action = np.random.choice(max_value.flatten())
          else:
            action = max_value[0]

        # if we don't exploit, pick one at random
        else:
          #print("Exploring")
          action = np.random.choice(range(10))

        # after choosing an action, get a reward
        R = np.random.randn() + q_star[action]
        #print('Action: %d Reward: %f' % (action, R))

        # then, update the agent's data
        N[action] += 1
        Q[action] += 1 / N[action] * (R - Q[action])

        # and save data for processing
        agents_data[epsilons.multi_index[0], agents.multi_index[0], steps.multi_index[0], :] = [action, R]
  # save calcs to a file
  np.save(path, agents_data)
  return agents_data

def main():

  no_arms = 10
  no_steps = 1000
  no_agents = 2000
  epsilon_steps = np.array([0, 0.01, 0.1])
  filepath = "./data/agent_stationary.npy"
  ### action values from random distribution for 10 actions
  if os.path.exists(filepath):
    agents_data = np.load(filepath)
  else:
    agents_data = save_agent_data(no_arms, no_steps, no_agents, epsilon_steps, filepath)


  for i in range(len(epsilon_steps)):
    plt.plot(agents_data[i, 0:, 0:, 1].mean(0), label = 'epsilon: ' + str(epsilon_steps[i]))

  plt.legend()
  plt.xlabel('No. of steps')
  plt.ylabel('Avg. reward')
  plt.title('Average agent performance on the %d armed bandit testbed\nN = %d; Stationary model' % (no_arms, no_agents))
  plt.show()

  #print(agents_data)
  #print(agents_data[0,0,:], agents_data[0,1,:], agents_data[0,2,:])

# This is the standard boilerplate that calls the main() function.
if __name__ == '__main__':
  main()
